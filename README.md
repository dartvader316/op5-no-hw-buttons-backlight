# READ CAREFULLY !!!
# Description
This module disables HW buttons backlight on OnePlus 5.

# Warnings
Flash at your own risk we are not responsible for any bricked devices

# Requirements
- OnePlus 5
- Latest Magisk v21+ (v22+ highly recommended)

# Installation
- Use make to create an archive to flash
- Flash via Magisk Manager v21+

# Uninstall
Uninstall or Remove via Magisk Manager Modules tab
If something goes wrong delete folder inside /data/adb/modules via TWRP file manager

[*] Flash at your own risk we are not responsible for any bricked devices
